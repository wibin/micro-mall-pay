module gitee.com/cristiane/micro-mall-pay

go 1.13

require (
	gitee.com/kelvins-io/common v1.0.2
	gitee.com/kelvins-io/kelvins v1.2.2
	gitee.com/kelvins-io/service-config v1.0.0 // indirect
	github.com/RichardKnop/machinery v1.9.1
	github.com/golang/protobuf v1.4.2
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/grpc-ecosystem/grpc-gateway v1.14.7
	github.com/satori/go.uuid v1.2.0
	github.com/shopspring/decimal v1.2.0
	github.com/tidwall/gjson v1.6.3 // indirect
	go.elastic.co/apm/module/apmgorm v1.9.0 // indirect
	go.elastic.co/apm/module/apmgrpc v1.9.0 // indirect
	go.elastic.co/apm/module/apmredigo v1.9.0 // indirect
	golang.org/x/net v0.0.0-20201016165138-7b1cca2348c0
	google.golang.org/grpc v1.32.0
	google.golang.org/protobuf v1.25.0
	xorm.io/xorm v1.0.3
)
